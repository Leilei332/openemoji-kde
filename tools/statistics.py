from pyecharts.charts import Pie
import os

namelist = os.listdir('../openmoji-svg')
stat = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
for i in namelist:
    name = list(i)
    num = name.count('-')
    stat[num] += 1
attr = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
pie = Pie()
print(stat)
print(namelist)
pie.add(series_name='Statistics',data_pair=list(zip(attr,stat)))
pie.render(path='statistics.html')
