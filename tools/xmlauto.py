from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement
from xml.etree.ElementTree import ElementTree
from xml.dom.minidom import Document
emoticonmap=Element('messaging-emoticon-map')
import os
files=os.listdir('../openmoji-svg')
files.remove('emoticons.xml')
for i in files:
    filename=os.path.splitext(i)[0]
    namelist=filename.split('-')
    chara = ''
    for j in namelist:
        chara+=chr(int(j,16))
    emoticon=SubElement(emoticonmap,'emoticon')
    emoticon.set('file',i)
    stri=SubElement(emoticon,'string')
    stri.text=chara
tree=ElementTree(emoticonmap)
tree.write('../openmoji-svg/emoticons.xml',encoding='utf-8')

'''
root=Element('messaging-emoticon-map')
emoticon=SubElement(root,'emoticon')
emoticon.set('file','00a9')
str1=SubElement(emoticon,'string')
str1.text='©'
str2=SubElement(emoticon,'string')
str2.text=':copyright:'
tree=ElementTree(root)
tree.write('sample.xml',encoding='utf-8')
'''